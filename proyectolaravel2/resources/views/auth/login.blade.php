<!-- resources/views/auth/login.blade.php -->

<!DOCTYPE html>
<html>
    <head>
        <title>
		@section('title')
		Panel
		@show
		</title>
		<meta charset="utf-8">
     
		@section('styles')
            <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
			<link rel="stylesheet" href="{{ asset('css/tiempo.css') }}" />
			<link rel="stylesheet" href="{{ asset('css/form.css') }}" />
		@show
        
    </head>
    <body>
        <div class="container">
            <div class="content">

		@if (Auth::guest())
		<div class="form-style-1">
		<form method="POST" action="login">
			{!! csrf_field() !!}
			<div>
				Email<br>
				<input type="email" name="email" value="{{ old('email') }}">
			</div>

			<div>
				Password<br>
				<input type="password" name="password" id="password">
			</div>

			<!--<div>
				<input type="checkbox" name="remember"> Remember Me
			</div>-->

			<div>
				<input type="submit" value="Iniciar sesión">
				
			</div>
			

		</form>
		</div>

		@else
			<p>Bienvenido {{ Auth::user()->name }}</p>
		@endif

		</div>
		
    </body>
</html>