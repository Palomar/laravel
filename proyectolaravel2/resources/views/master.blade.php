<!DOCTYPE html>
<html>
    <head>
        <title>
		@section('title')
		Panel
		@show
		</title>
		<meta charset="utf-8">
     
		@section('styles')
            <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
			<link rel="stylesheet" href="{{ asset('css/tiempo.css') }}" />
			<link rel="stylesheet" href="{{ asset('css/form.css') }}" />
		@show
        
    </head>
    <body>
        <div class="container">
            <div class="content">
			<img class="icono" src="{{ asset('/img/icono.png') }}">
                <div class="menu-option">
				
				@if (Auth::check())
				{{ Auth::user()->name }}
				@else
					<p>Welcome guest!</p>
				@endif
				</div>
				
				<div class="menu-option">
					<a href="{{ url('/') }}">Inicio</a>
				</div>
				
				@if (Auth::user()->name === 'admin')
				<div class="menu-option">
					<a href="{{ url('alta-empleado') }}">Alta empleado</a>
				</div>
				
				
				<div class="menu-option">
					<a href="{{ url('lista-empleados') }}">Listado de empleados</a>
				</div>
				@endif
				
				@if (Auth::check())
				<div class="menu-option">
					<a href="{{ url('password') }}">Cambiar contraseña</a>
				</div>

				<div class="menu-option">
					<a href="{{ url('lista-partes') }}">Listado de partes</a>
				</div>

				<div class="menu-option">
					<a href="{{ url('alta-parte') }}">Alta de parte</a>
				</div>				
				
				<div class="menu-option">
					<a href="{{ url('logout') }}">Log out</a>
				</div>
				@endif
            </div>
			<div class="content">
			@yield('content')
			</div>
        </div>
		
    </body>
</html>
