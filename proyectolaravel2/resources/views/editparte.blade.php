@extends('master')

@section('content')

@if($errors->any())
	<div class="row collapse">
		<ul class="alert-box warning radius">
			@foreach($errors->all() as $error)
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	</div>
@endif
<div class="form-style-1">
<form method="POST" action="{{ url('editar-parte') }}/{{$parte->id}}">
    {!! csrf_field() !!}
    <div>
        Observación<br>
        <textarea name="observacion">{{ $parte->observacion }}</textarea>
    </div>
	
    <div>
        Tiempo de inicio<br>
        <input type="datetime-local" name="tiempo_ini" value="{{ $parte->tiempo_ini }}" 
		min="2017-04-12T12:00:00" required placeholder="AAAA-MM-DD HH:MM:SS">
    </div>
	
	<div>
        Tiempo de fin<br>
        <input type="datetime-local" name="tiempo_fin" value="<?php echo ($parte->tiempo_fin != '0000-00-00 00:00:00') ? $parte->tiempo_fin : "" ?>" 
		min="2017-04-12T12:00:00" placeholder="AAAA-MM-DD HH:MM:SS">
    </div>

    <div>
        Desplazamiento
		<input type="checkbox" name="desplazamiento" checked value="0" hidden>
		@if ($parte->desplazamiento === 1)
        <input type="checkbox" name="desplazamiento" checked value="1">
		@else
		<input type="checkbox" name="desplazamiento" value="1">	
		@endif
    </div>
    <input type="text" name="id_usuario" value="{{ $parte->id_usuario }}" hidden>
    <div>
        <input type="submit" value="Modificar">
    </div>
</form>
</div>
@stop