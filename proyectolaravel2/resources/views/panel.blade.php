        @extends('master')
		
		
		
		@section('content')

		<div>
		@if (Auth::check())
		<p>Bienvenido {{ Auth::user()->name }}</p>
		<p>
		<div class="datagrid tiempo-table tiempo-table-width">
			<table>
			<thead>
				<tr>
				<th class="tiempo-table">Tiempo total<br>Horas:Minutos:Segundos </th>
				</tr>
			</thead>
			<tr>
				<td  class="tiempo-table">{{ $tiempo or "Sin tiempo" }}</td>
			</tr>
			
			</table>
		</div>
		</p>
		@else
			<p>Welcome guest!</p>
		@endif
		</div>
		
		
		
		@stop
	