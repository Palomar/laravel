
        @extends('master')
		
		@section('content')
		
		<div class="form-style-1">
		<form method="POST" action="lista-empleados">
		{!! csrf_field() !!}
		<input type="text" name="search" placeholder="Buscar...">
		<input type="submit" value="Enviar" >
		</form>
		</div>
		
		<div class="datagrid tiempo-table table-width2">		
		<table>
			<thead>
			<tr>
				<th>Nombre</th>
				<th>Correo electrónico</th>
				<th>Estado empleado</th>
				<th>Modificar</th>
				
			</tr>
			</thead>
			<tbody>
		@foreach($empleados as $emp)
			
			<tr>
				<td>{{ $emp->name }}</td>
				<td>{{ $emp->email }}</td>
				<td class="td-width">
				@if ($emp->activo === 1) 
				<input type="checkbox" name="activo" checked value="1" disabled>
				@else 
				<input type="checkbox" name="activo" value="0" disabled>
				@endif
				</td>
				<td class="td-width">
				<a href="editar-empleado/{{ $emp->id }}">Editar</a>
				</td>
			</tr>
		@endforeach
		</tbody>
		
		@if ($empleados->render())
		<tfoot>
			<tr>
			<td colspan="4">
			<div class="pagination">
		
		{!! $empleados->render() !!}
		
		</div>
			<td>
			</tr>
		</tfoot>
		@endif
		
		</table>
		</div>
		
		@stop