
        @extends('master')
		
		@section('content')
		
		<div>
		@if ($partes->count())
		<div class="form-style-1">
		
		<form method="POST" action="{{ url('lista-partes') }}">
		{!! csrf_field() !!}
		<input type="text" name="search" placeholder="Buscar...">
		
		<input type="submit" value="Enviar" >
		<input type="checkbox" name="cerrado" checked value="0" hidden>
		<input type="checkbox" name="cerrado" value="1">	
		Partes no cerrados
		<input type="checkbox" name="nombre" checked value="0" hidden>
		<input type="checkbox" name="nombre" value="1">	
		Buscar por nombre
		</form>
		
		</div>
		<div class="datagrid tiempo-table table-width">		
		<table>
			<thead>
			<tr>
				<th>Nombre</th>
				<th>Observación</th>
				<th>Inicio</th>
				<th>Fin</th>
				<th>Desplazamiento</th>
				<th>Modificar</th>
				@if(Auth::user()->name === 'admin')
				<th>Eliminar</th>
				@endif
			</tr>
			</thead>
			<tbody>
		
		@foreach($partes as $parte)
			
			<tr>
				<td>{{ $parte->name }}</td>
				<td>{{ $parte->observacion }}</td>
				<td class="td-width">{{ $parte->tiempo_ini }}</td>
				<td class="td-width">{{ $parte->tiempo_fin }}</td>
				
				<td class="td-width">
				@if ($parte->desplazamiento === 1) 
				<input type="checkbox" name="activo" checked value="1" disabled>
				@else 
				<input type="checkbox" name="activo" value="0" disabled>
				@endif
				</td>
				<td>
				<a href="editar-parte/{{ $parte->id }}">Editar</a>
				</td>
				@if(Auth::user()->name === 'admin')
				<td>
				<a href="eliminar-parte/{{ $parte->id }}">Borrar</a>
				</td>
				@endif
			</tr>
		@endforeach
		
		</tbody>
		@if ($partes->render())
		<tfoot>
			<tr>
			<td colspan="6">
			<div class="pagination">
		
		{!! $partes->render() !!}
		
		</div>
			<td>
			</tr>
		</tfoot>
		@endif
		</table>
		</div>
		@else
			<tr>
				<td colspan="6">No tiene ningún parte realizado.</td>
			</tr>
		@endif
		</div>
		
		@stop