@extends('master')

@section('content')

@if($errors->any())
	<div class="row collapse">
		<ul class="alert-box warning radius">
			@foreach($errors->all() as $error)
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	</div>
@endif
<div class="form-style-1">
<form method="POST" action="{{ url('editar-empleado') }}/{{$empleado->id}}">
    {!! csrf_field() !!}
    <div>
        Nombre<br>
		@if ($empleado->name === 'admin') 
        <input type="text" name="name" value="{{ $empleado->name }}" disabled>
		@else
		<input type="text" name="name" value="{{ $empleado->name }}">
		@endif
    </div>

    <div>
        Correo electrónico<br>
        <input type="email" name="email" value="{{ $empleado->email }}">
    </div>

    <div>
        Activar empleado
		<input type="checkbox" name="activo" checked value="0" hidden>
		@if ($empleado->name === 'admin') 
		<input type="checkbox" name="activo" checked value="1" disabled>
		@elseif ($empleado->activo === 1)
		<input type="checkbox" name="activo" checked value="1">
		@else 
		<input type="checkbox" name="activo" value="1">
		@endif
	</div>
    
    <div>
        <input type="submit" value="Modificar">
    </div>
</form>
</div>
@stop