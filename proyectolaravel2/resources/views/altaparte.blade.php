@extends('master')

@section('content')

@if($errors->any())
	<div class="row collapse">
		<ul class="alert-box warning radius">
			@foreach($errors->all() as $error)
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	</div>
@endif
<div class="form-style-1">
<form method="POST" action="{{ url('alta-parte') }}">
    {!! csrf_field() !!}
    <div>
        Observación<br>
        <textarea name="observacion" ></textarea>
    </div>
	
    <div>
        Tiempo de inicio<br>
        <input type="datetime-local" name="tiempo_ini" value="<?php echo (new \DateTime())->format('Y-m-d H:i:s'); ?>" 
		min="2017-04-12T12:00:00" required placeholder="AAAA-MM-DD HH:MM:SS">
    </div>
	
	<div>
        Tiempo de fin<br>
        <input type="datetime-local" name="tiempo_fin" value="<?php echo (new \DateTime())->format('Y-m-d H:i:s'); ?>" 
		min="2017-04-12T12:00:00" placeholder="AAAA-MM-DD HH:MM:SS">
    </div>

    <div>
        Desplazamiento
		<input type="checkbox" name="desplazamiento" checked value="0" hidden>
        <input type="checkbox" name="desplazamiento" checked value="1">
    </div>
    
    <div>
		<input type="submit" value="Registrar">
        
    </div>
</form>
</div>
@stop