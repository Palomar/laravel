@extends('master')

@section('content')

@if($errors->any())
	<div class="row collapse">
		<ul class="alert-box warning radius">
			@foreach($errors->all() as $error)
				<li> {{ $error }} </li>
			@endforeach
		</ul>
	</div>
@endif
<div class="form-style-1">
<form method="POST" action="{{ url('alta-empleado') }}">
    {!! csrf_field() !!}
    <div>
        Nombre<br>
        <input type="text" name="name" value="{{ old('name') }}">
    </div>

    <div>
        Correo electrónico<br>
        <input type="email" name="email" value="{{ old('email') }}">
    </div>

    <div>
        Activar empleado
		<input type="checkbox" name="activo" checked value="0" hidden>
        <input type="checkbox" name="activo" checked value="1">
    </div>
    
    <div>
        <input type="submit" value="Registrar">
    </div>
</form>
</div>
@stop