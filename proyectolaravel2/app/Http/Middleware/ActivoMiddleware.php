<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ActivoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (Auth::check()){
			if ($request->user()->activo != 1)
			{
				return redirect('/login');
			}
		}else{
			return redirect('/');
		}
		
        return $next($request);
    }
}
