<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\Auth;
/*
Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('use/{id}', function($id) {
	echo "Hola $id";
})-> where ('id','[0-9]+');

Route::get('user/{id?}', function($id=null) {
	echo "$id";
});

Route::get('users/{user}', function (App\User $user) {
  return $user->email;
});*/

//Redirección en caso de falta de login
Route::get('/', function() {
	if (Auth::guest()) {
		return Redirect::to('login');
    }else{
		
		$array[0] = Auth::user()->id;
		$query = DB::select('SELECT SUM(IFNULL( TIMESTAMPDIFF(SECOND, tiempo_ini, tiempo_fin), 0 ) )
		AS suma FROM partes WHERE id_usuario = ?', $array);
		$tiempo = $query[0]->suma;
		return View::make('panel')->with('tiempo', toTime($tiempo));
		
    }
});

/*Route::get('alta-empleado', function() {
	if (Auth::guest()) {
		return Redirect::to('login');
    }else{
		return view('altauser');
    }
});*/

/*Route::group(array('before' => 'auth'), function() 
{
    Route::get('alta-empleado', array(
		'uses' => 'Auth\AuthController@getRegister',
	));
	
	Route::post('alta-empleado', array(
		'uses' => 'Auth\AuthController@postRegister',
	));
	

    // More Routes

});*/

Route::group(['middleware' => 'auth', 'middleware' => 'App\Http\Middleware\AdminMiddleware'], function () {
	//if (Auth::check() && Auth::user()->name == 'admin'){
	
	Route::get('alta-empleado', 'UserController@create');
	Route::post('alta-empleado', 'UserController@store');
	Route::get('lista-empleados', 'UserController@index');
	Route::post('lista-empleados', 'UserController@search');
	Route::get('editar-empleado/{id}', 'UserController@show')->where(['id' => '[0-9]+']);
	Route::post('editar-empleado/{id}', 'UserController@edit')->where(['id' => '[0-9]+']);
	Route::get('eliminar-parte/{id}', 'ParteController@destroy')->where(['id' => '[0-9]+']);
});

Route::group(['middleware' => 'auth', 'middleware' => 'App\Http\Middleware\ActivoMiddleware'], function () {
	
	Route::get('password', 'UserController@password');
	Route::post('password', 'UserController@editPassword');
	
	Route::get('alta-parte', 'ParteController@create');
	Route::post('alta-parte', 'ParteController@store');
	Route::get('editar-parte/{id}', 'ParteController@show')->where(['id' => '[0-9]+']);
	Route::post('editar-parte/{id}', 'ParteController@edit')->where(['id' => '[0-9]+']);
	Route::get('lista-partes', 'ParteController@index');
	Route::post('lista-partes', 'ParteController@search');
});

// Authentication routes...
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

// Registration routes...
/*Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@postRegister');*/

/*Route::get('partes', function(){
	dd(\App\Parte::all());
});*/

function toTime($tiempo){
	$num = gmdate("H:i:s", intval($tiempo));
	return $num;
}