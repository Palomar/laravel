<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parte;
use Validator;
use DB;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ParteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$partes = DB::table('partes')->paginate(10);
		if (Auth::user()->name == 'admin'){
			$query = DB::table('users')
			->join('partes','partes.id_usuario','=','users.id');
			
		}else {
			$query = DB::table('users')
			->join('partes','partes.id_usuario','=','users.id')
			->where(['users.id' => Auth::user()->id]);
			
		}
		$partes = $query->paginate(10);
		//->select('users.*', 'partes.*')
		//->pagination(10);
		//->get();
		
		/*$partes = DB::select(DB::raw('SELECT * FROM users JOIN partes ON users.id = partes.id_usuario'));
		$partes = $partes->paginate(5, array('users.*', 'partes.*'));*/
		
		
		//$pagination = Paginator::make($all_transactions, count($all_transactions), $results_per_page);	
		
		//->select('users.id','users.name')->join('partes','partes.id_usuario','=','users.id')->get();
		//->where(['something' => 'something', 'otherThing' => 'otherThing'])->get();
		return view('listapartes', ['partes' => $partes]); //->toArray()]);
    }

	public function search(Request $request)
    {
		$search = $request['search'];
		$cerrado = $request['cerrado'];
		$nombre = $request['nombre'];
		
		if (Auth::user()->name == 'admin'){
			$query = DB::table('users')
			->join('partes','partes.id_usuario','=','users.id')
			->where('observacion','like','%'.$search.'%');
			if($nombre){
				$query = $query->orWhere('name','like','%'.$search.'%');
			}
			//->orWhere('name','like','%'.$search.'%');
			if($cerrado){
				//Busca partes no cerrados
				$query = $query->where(['tiempo_fin' => '0000-00-00 00:00:00']);
			}
			$query = $query->orderBy('tiempo_ini');
			
		}else {
			$query = DB::table('users')
			->join('partes','partes.id_usuario','=','users.id')
			->where('observacion','like','%'.$search.'%')
			->where(['users.id' => Auth::user()->id]);
			if($cerrado){
				//Busca partes no cerrados
				$query = $query->where(['tiempo_fin' => '0000-00-00 00:00:00']);
			}
			$query = $query->orderBy('tiempo_ini');
			
		}
		$partes = $query->paginate(10);
		
		return view('listapartes', ['partes' => $partes]); //->toArray()]);
    }
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('altaparte');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        
		Parte::create([
			'id_usuario' => Auth::user()->id,
            'observacion' => $request['observacion'],
            'tiempo_ini' => $request['tiempo_ini'],
            'tiempo_fin' => $request['tiempo_fin'],
			'desplazamiento' => $request['desplazamiento'],
        ]);
		
        //return redirect($this->redirectPath());
		return redirect('lista-partes');
    }

	
	protected function validator(array $data)
    {
		return Validator::make($data, [
            'observacion' => 'required',
            'desplazamiento' => 'required|boolean', //required|email|max:255|unique:users',
			'tiempo_ini' => 'required|date_format:Y-m-d H:i:s',
			'tiempo_fin' => 'sometimes|date_format:Y-m-d H:i:s',
		]);
    }
	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $parte = Parte::find($id);
		if (!is_null($parte)){
			if ((Auth::user()->id == $parte->id_usuario) || Auth::user()->name == 'admin'){
				return view('editparte', ['parte' => $parte]);
			}else{
				return response('Intentas manipular indebidamente la url, han sido almacenados tus datos y se dará traslado a la policía judicial para investigar este intento de suplantación. Saludos a la GC.', 404);
			}
		}else{
			return response('No encontrado', 404);
		}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $validator = $this->validator($request->all());
		
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
		if ((Auth::user()->id == $request['id_usuario']) || Auth::user()->name == 'admin'){
		Parte::where('id', $id)->update(['observacion'=>$request['observacion'],
		'tiempo_ini'=>$request['tiempo_ini'], 'tiempo_fin'=>$request['tiempo_fin'],
		'desplazamiento'=>$request['desplazamiento']]);
		return redirect('lista-partes');
		}else{
			return response('Intentas manipular indebidamente la url, han sido almacenados tus datos y se dará traslado a la policía judicial para investigar este intento de suplantación. Saludos a la GC.', 404);
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		if (Auth::user()->name == 'admin'){
			$parte = Parte::find($id);
			$parte->delete();
			return redirect('lista-partes');
		}else{
			return response('Intentas manipular indebidamente la url, han sido almacenados tus datos y se dará traslado a la policía judicial para investigar este intento de suplantación. Saludos a la GC.', 404);
		}
        
    }
}
