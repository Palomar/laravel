<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use DB;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	
	protected $redirectPath = '/';
	
	
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$empleados = User::all()->simplePaginate(2);
		$empleados = DB::table('users')->paginate(10);
		return view('listausers', ['empleados' => $empleados]); //->toArray()]);
    }
	
	public function search(Request $request)
    {
		$search = $request['search'];
		$empleados = User::where('name','like','%'.$search.'%')
		->orWhere('email','like','%'.$search.'%')
        ->orderBy('name')
        ->paginate(100);
		return view('listausers', ['empleados' => $empleados]); //->toArray()]);
    }
	
	protected function validator(array $data)
    {
		return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
			
		]);
    }
	
	protected function validatorUpdate(array $data, $id)
	{
		$user = User::find($id);
		return Validator::make($data, [
            'name' => 'required|max:255',
			'email' => 'required|email|max:255',
            //'email' => 'sometimes|required|email|max:255|unique:users, email,'.$id // ',id',
			//email|unique:users, email, {{$id}}, user_id
		]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('altauser');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        //User::create($request->all());
		User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt('123456'),
			'activo' => $request['activo'],
        ]);
		
        //return redirect($this->redirectPath());
		return redirect('lista-empleados');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empleado = User::find($id);
		if (!is_null($empleado))
			return view('edituser', ['empleado' => $empleado]);
		else
			return response('No encontrado', 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
		$validator = $this->validatorUpdate($request->all(), $id);
		
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
		
		//$id = $request['id'];
		
		User::where('id', $id)->update(['name'=>$request['name'], 'email'=>$request['email'], 'activo'=>$request['activo']]);
		return redirect('lista-empleados');
    }
	
	public function password()
    {
        return view('password');
	}
	
	/**
	
	Cambia la contraseña
	
	*/
	public function editPassword(Request $request)
    {
		$pass = bcrypt($request['password']);
		$id = Auth::user()->id;
		User::where('id', $id)->update(['password'=> $pass]);
		return redirect('lista-empleados');
    }
	
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
