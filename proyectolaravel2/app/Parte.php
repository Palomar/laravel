<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parte extends Model
{
    protected $table = 'partes';
	
	protected $fillable = ['observacion', 'id_usuario', 'tiempo_ini', 'tiempo_fin', 'desplazamiento'];
}
