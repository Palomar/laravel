<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' 				=>	'admin',
        	'email'				=>	'mail@maildefault.com',
        	'password'			=>	'$2y$10$VgVRZFyiQ3dldvFp6cYVluvLkcX1xq8pk6OWeAT.mP8txaeX1f3H2',
			'activo'			=>	true,
        ]);
    }
}
