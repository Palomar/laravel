<?php

use Illuminate\Database\Seeder;
use App\Parte;

class PartesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Parte::create([
			'tiempo_ini'		=> 	date_create('2017-04-10 19:00:00')->format('Y-m-d H:i:s'),
        	'tiempo_fin'		=> 	date_create('2017-04-10 21:00:00')->format('Y-m-d H:i:s'),
			'observacion'		=>	'prueba',
        	'desplazamiento'	=>	false,
        	'id_usuario'		=>	1
        ]);
		
		Parte::create([
			'tiempo_ini'		=> 	date_create('2017-04-09 09:00:00')->format('Y-m-d H:i:s'),
        	'tiempo_fin'		=> 	date_create('2017-04-09 15:00:00')->format('Y-m-d H:i:s'),
			'observacion'		=>	'prueba',
        	'desplazamiento'	=>	true,
        	'id_usuario'		=>	1
        ]);
    }
}
