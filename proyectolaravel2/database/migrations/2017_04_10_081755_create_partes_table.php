<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partes', function (Blueprint $table) {
            $table->increments('id');
			$table->dateTime('tiempo_ini');
			$table->dateTime('tiempo_fin')->nullable();
			$table->text('observacion')->nullable();
			$table->boolean('desplazamiento');
			$table->integer('id_usuario')->unsigned();;
			$table->foreign('id_usuario')->references('id')->on('users');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partes', function (Blueprint $table) {
            Schema::drop('partes');
        });
    }
}
