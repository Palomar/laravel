Para cargar los datos por defecto que crean la BBDD y el usuario "admin" que permite administrar:

Crear una "BBDD" en la mysql de nombre "laravel" y luego ejecutar en la consola:

php artisan migrate:refresh --seed

Despu�s para loguear con "admin", se usa la contrase�a "123456" que est� encriptada en la BBDD,
y el nombre de loguin que se usa es el correo "mail@maildefault.com", desde el fichero

(ruta)/proyectolaravel2/database/seeds/UserSeeder.php

se puede cambiar el seeder con otro correo.

Todos los usuarios se loguean con su respectivo correo.
Si no se quiere cerrar un parte, con dejar el campo de fecha fin vac�o es suficiente.
La contrase�a por defecto cuando creamos un nuevo usuario es "123456".

Informaci�n extra:

-Se ha controlado que las rutas solo sean accesibles en funci�n de estar logueado.
-Que las rutas est�n protegidas por los permisos de administrador si proceden.
-Que no se puedan manipular la ruta para realizar inserciones o modificaciones de usuarios que no tiene permisos.
-Que se introducen datos v�lidos de correo electr�nico o de fechas en los partes.
-Que el correo electr�nico es �nico (salvo cuando lo editamos por el administrador, quien puede alterar esto)
-Que el administrador no puede cambiarse el nombre a diferencia del resto de usuarios ya que podria afectar
al funcionamiento de permisos del middleware.
-Que un usuario desactivado por el admin, puede loguearse en la portada pero no tendr� acceso al resto de funcionalidades
pues se supone que es un empleado dado de baja dentro del sistema, pero se retiene su existencia para el control de partes.